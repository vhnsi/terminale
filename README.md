# Terminale

Cours sur les protocoles de routages pour les terminales NSI.

- [modèle osi et tcp ip](modele_tcp_ip.pdf)
- [adresse IP](adresse_ip-ipv6.pdf)
- [exercice table de routage](exo_table.pdf)
- [table de routage](table_de_routage.pdf)
- [exercice protocole de routage 1](Exercice.pdf)
- [exercice protocole de routage 2](exo_suite.pdf)
- [TP filius](tp_filius.pdf)
